import request from '@/utils/request'

// 查询咨询详情
export function getInformation(information_id,page,size) {
  return request({
    // url: '/saas/mall/area/query',
    url: `/bbs/api/information/${information_id}?page=${page}&size=${size}`,
    method: 'get',
  }).catch((e) => {
    return e
  })
}

// 点赞
export function thumbUp(pid) {
  return request({
    url: `/bbs/api/posts/${pid}/like`,
    method: 'get',
  })
}
