import axios from 'axios'
// import { MessageBox, Message } from 'element-ui'
// import store from '@/store'
import { getToken } from '@/utils/auth'
import CreateSign, { APP_KEY } from '@/utils/sign'

// create an axios instance
const service = axios.create({
  // baseURL: 'https://222.217.61.75:443', // url = base url + request url
  baseURL: process.env.API_ROOT, // url = base url + request url
  // baseURL: 'http://113.17.111.41:8866', // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 15000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    const createSign = new CreateSign()
    let SIGN
    if (config.params) {
      SIGN = createSign.createMd5Sign(config.params)
    } else if (config.data) {
      SIGN = createSign.createMd5Sign(config.data)
    } else {
      SIGN = createSign.createMd5Sign()
    }

    // 统一设置全局headers
    config.headers = {
      Authorization: 'Bearer ' + getToken(), // 让每个请求携带自定义token
      // sign: SIGN,
      // key: APP_KEY,
      // ts: createSign.TS,
      // code: createSign.CODE
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data
    // console.log(res)
    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 200) {
      // Message({
      //   message: res.message || 'error',
      //   type: 'error',
      //   duration: 5 * 1000
      // })

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 401 || res.code === 403) {
        // to re-login
        // MessageBox.confirm('你已被登出，可以取消继续留在该页面，或者重新登录', '确定登出', {
        //   confirmButtonText: '重新登录',
        //   cancelButtonText: '取消',
        //   type: 'warning'
        // }).then(() => {
        //   store.dispatch('user/resetToken').then(() => {
        //     location.reload()
        //   })
        // })
      }
      return Promise.reject(res.message || 'error')
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    // Message({
    //   message: error.message,
    //   type: 'error',
    //   duration: 5 * 1000
    // })
    return Promise.reject(error)
  }
)

export default service
