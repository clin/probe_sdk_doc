import storage from 'good-storage'

const TokenKey = 'BEARER_AUTHORIZATION'

export function getToken() {
  return storage.get(TokenKey)
  // return window.localStorage.getItem(TokenKey)
}

export function setToken(token) {
  return storage.set(TokenKey,token)
  // return window.localStorage.setItem(TokenKey, token)
}

export function removeToken() {
  return storage.remove(TokenKey)
  // return window.localStorage.removeItem(TokenKey)
}
