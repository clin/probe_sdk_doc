

### 集成方式选择

神策分析 SDK 支持 framework 方式集成。

#### 通过framework集成

- 将 SensorsAnalyticsSDK.framework 导入 App 项目，并选中 `Copy items if needed`;
- 项目设置 "Build Phase" -> "Link Binary With Libraries" 中添加依赖库：`libicucore`、`libsqlite3` 和 `libz`。

注1：集成 SDK 会使 App 安装包体积增加约 350KB。
注2：神策 SDK 只兼容 iOS 7.0 及以上版本。



### 初始化 SDK 及基本配置

注意：需要在 程序入口 主线程 同步 初始化 SDK，否则可能会丢失部分事件数据。

```Objective-C
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // 初始化配置，必选
    SAConfigOptions *options = [[SAConfigOptions alloc] initWithServerURL:<#数据接收地址#> launchOptions:launchOptions];

    // 开启全埋点，非必选
    options.autoTrackEventType = SensorsAnalyticsEventTypeAppStart |
                                 SensorsAnalyticsEventTypeAppEnd |
                                 SensorsAnalyticsEventTypeAppClick |
                                 SensorsAnalyticsEventTypeAppViewScreen;

    // 开启 crash 采集，非必选
    options.enableTrackAppCrash = YES;

    // 初始化 SDK，必选
    [SensorsAnalyticsSDK startWithConfigOptions:options];

    ** 注意：只有初始化 SDK 后，才可以调用 [SensorsAnalyticsSDK sharedInstance] 进行其他操作 **
    /**
     SDK 其他操作，如：注册公共属性、track 激活事件、App 与 H5 打通、开启热力图等
     */

    return YES;
}
```

Swift 代码示例：

```Swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    // 初始化配置，必选
    let options = SAConfigOptions(serverURL: <#数据接收地址#>, launchOptions: launchOptions)

    // 开启全埋点，非必选
    options.autoTrackEventType = [.eventTypeAppClick,.eventTypeAppStart,.eventTypeAppEnd,.eventTypeAppViewScreen]

    // 开启 crash 采集，非必选
    options.enableTrackAppCrash = true

    // 初始化 SDK，必选
    SensorsAnalyticsSDK.start(configOptions: options)

    ** 注意：只有初始化 SDK 后，才可以调用 SensorsAnalyticsSDK.sharedInstance() 进行其他操作 **
    /**
     SDK 其他操作，如：注册公共属性、track 激活事件、App 与 H5 打通、开启热力图等
     */
    return true
}
```

- [1.10.26](https://github.com/sensorsdata/sa-sdk-ios/releases/tag/v1.10.26)及之前的版本，SDK 初始化方式： **查看示例**





### 处理传入的 URL

AppDelegate 中的 `-application:openURL:options:` 方法中 调用 `-handleSchemeUrl:` 对神策分析 scheme 进行处理。

```Objective-C
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    if ([[SensorsAnalyticsSDK sharedInstance] handleSchemeUrl:url]) {
        return YES;
    }
    return NO;
}
```

*注1*：如果 `-application:openURL:options:` 中还有其他逻辑处理，需要将 `-handleSchemeUrl:` 尽量前置，以保证 `-handleSchemeUrl:` 能被执行。



### 代码埋点追踪事件

神策分析 SDK 成功初始化后，可以通过 `track:` 和 `track:withProperties:` 方法追踪用户行为事件，并为事件添加自定义属性。

以电商产品为例，可以这样追踪一次购物行为：

```Objective-C
UInt64 productId = 123456;
NSString *productCatalog = @"Laptop Computer";
BOOL isAddedToFavorites = NO;

[[SensorsAnalyticsSDK sharedInstance] track:@"ViewProduct"
                             withProperties:@{@"ProductID" : [NSNumber numberWithUnsignedLong:productId],
                                              @"ProductCatalog" : productCatalog,
                                              @"IsAddedToFav" : isAddedToFavorites ? @YES : @NO}];
```

Swift 代码示例：

```Swift
SensorsAnalyticsSDK.sharedInstance()?.track("ViewProduct",withProperties:["ProductID":123456,
                                                                          "ProductCatalog":"Laptop Computer",
                                                                          "IsAddedToFav":false])
```

注1. 事件名和属性名需要时合法变量名，且不能以 `$` 开头。 注2. 属性名大小写敏感，并且不同事件会共用同名属性。即若 foo 事件含有 "aaa" 属性，则后续所有事件**不能**使用与 "aaa" 仅大小写不同的属性(如 aAa、aaA)。 注3. 事件名和属性其他约束，具体请参考 [数据格式](https://www.sensorsdata.cn/manual/data_schema.html)

### 配置全埋点

SDK 可以自动采集一些通用的用户行为，包括 App 的启动、退出，页面浏览，元素点击四种事件。开发者可以通过 `autoTrackEventType` 来选择性开启 SDK 自动采集功能:

```Objective-C
SAConfigOptions *options = [[SAConfigOptions alloc] initWithServerURL:<#数据接收地址#> launchOptions:launchOptions];
// 需要开启的全面点类型
options.autoTrackEventType = SensorsAnalyticsEventTypeAppStart | SensorsAnalyticsEventTypeAppEnd | SensorsAnalyticsEventTypeAppClick | SensorsAnalyticsEventTypeAppViewScreen;
[SensorsAnalyticsSDK startWithConfigOptions:options];
```

Swift 代码示例：

```Swift
let options = SAConfigOptions.init(serverURL:<#数据接收地址#>, launchOptions: launchOptions)
// 需要开启的全面点类型
options.autoTrackEventType = [.eventTypeAppClick,.eventTypeAppStart,.eventTypeAppEnd,.eventTypeAppViewScreen]
SensorsAnalyticsSDK.start(configOptions: options)
```

- [1.10.26](https://github.com/sensorsdata/sa-sdk-ios/releases/tag/v1.10.26)及之前的版本，SDK 开启自动采集功能： **查看示例**





### 记录激活事件

如果使用了神策渠道追踪功能，则需要在 App 启动时调用 `trackInstallation:` 记录安装激活事件(SDK 内部做了处理，多次调用此方法，只有第一次生效)。

```Objective-C
// 事件名可以根据需要自定义，一般为 AppInstall
[[SensorsAnalyticsSDK sharedInstance] trackInstallation:@"AppInstall"];
```

*注1.* 此接口第一次调用时，会触发 AppInstall 事件，并设置 $first_visit_time 用户属性。 *注2.* 更多关于渠道追踪功能介绍请参考 [《新增用户与渠道追踪》](https://www.sensorsdata.cn/manual/track_installation.html)

### 设置事件公共属性

如果所有事件中都具有某个属性值，则可以将该属性作为公共属性，一个属性注册成公共属性后，SDK 自动会将该属性拼接到所有的埋点事件中，省去了每次都手动添加困扰。 可以通过 `registerSuperProperties:`注册事件公共属性。 例如将应用名称作为事件的公共属性：

```Objective-C
[[SensorsAnalyticsSDK sharedInstance] registerSuperProperties:@{@"AppName" : @"<#YOUR APP NAME#>"}];
```

成功设置事件公共属性后，再通过 `track:` 追踪事件时，事件公共属性会被添加进每个事件中，例如：

```Objective-C
// 记录收藏商品事件
[[SensorsAnalyticsSDK sharedInstance] track:@"AddToFav"
                             withProperties:@{@"ProductID" : [NSNumber numberWithUnsignedLong:123456]}];
```

在设置事件公共属性后，实际发送的事件中会被加入 `AppName` 属性，等价于

```Objective-C
// 记录收藏商品事件
[[SensorsAnalyticsSDK sharedInstance] track:@"AddToFav"
                             withProperties:@{
                                                 @"ProductID" : [NSNumber numberWithUnsignedLong:123456],
                                                 @"AppName" : @"<#YOUR APP NAME#>"
                                                 }];
```

Swift 代码示例：

```Swift
SensorsAnalyticsSDK.sharedInstance().registerSuperProperties(["AppName":"<#YOUR APP NAME#>"])
```

注1. 公共属性只会对注册之后的事件生效，所以需要尽早注册公共属性，建议在初始化 SDK 之后立即设置。 注2. 公共属性会保存在 App 本地缓存中。可以通过 `unregisterSuperProperty:` 删除一个公共属性，或使用 `clearSuperProperties:` 删除所有已设置的事件公共属性。
注3. 当公共属性和事件属性的 Key 相同时，事件属性优先级最高，它会覆盖公共属性。
注4. 公共属性约束和事件属性相同，详情参考[数据格式](https://www.sensorsdata.cn/manual/data_schema.html)

### ID 与用户标识

无论是自定义代码埋点事件还是全埋点自动采集的事件，神策中每个事件都会关联到一个 ID 上，用于标识该事件所对应的用户或设备信息，我们称之为 `distinct_id`。默认情况下，用户登录前，`distinct_id` 是 SDK 根据设备生成的一个 `匿名 ID`，一般为`IDFA`、`IDFV` 或 `UUID`;用户登录后，开发人员调用 `login:`将用户 `登录 ID` 传给 SDK，后续该设备上所有事件的 `distinct_id` 就会变成用户所对应的 `登录 ID`。

*注1.* 关于匿名 ID 的生成规则，可参考

#### ID 绑定和用户关联

用户登录前，`distinct_id` 为 `匿名 ID`；用户登录后，`distinct_id` 为 `登录 ID`。为了将用户登录前后的事件序列串联起来，神策分析采用了用户关联机制：调用 `login:` 接口时，后台会尝试将 `匿名 ID` 和 `登录 ID` 关联起来，并生成一个新的 `user_id`，并在神策分析中使用 `user_id` 作为标识用户和计算指标的依据。 SDK 内部做了逻辑处理，多次调用 `login:` 传入相同的 `登录 ID` 时，SDK 会进行忽略。为了保证用户关联的覆盖率，在如下时机时均需要调用 `login:` 进行用户关联：

1. 用户注册或登录成功时，需调用 `login:` 进行用户关联
2. App 启动时，若当前为已登录用户，需调用 `login:` 传入`登录 ID` 进行用户关联

Objective-C 代码示例：

```
[[SensorsAnalyticsSDK sharedInstance] login:@"<#登录ID#>"];
```

Swift 代码示例：

```Swift
SensorsAnalyticsSDK.sharedInstance().login("<#登录ID#>")
```

*注1.* 登录 ID 是指可以唯一标识一个用户的 ID，通常是业务数据库里的主键或其它唯一标识。

### 设置用户属性

除了给事件添加属性，SDK 也支持给用户设置属性，如年龄、性别、等级，神策事件分析、留存分析、分布分析等功能均支持使用用户属性作为过滤条件，精确分析特定人群的指标。 *注1.* 关于事件属性和用户属性的区别，请参考 [数据模型](https://www.sensorsdata.cn/manual/data_model.html)

- 更新用户属性 `set:`、`set:to:`，可以设定用户属性，同一个 key 多次设置时，value 值会进行覆盖替换。

Objective-C 代码示例：

```Objective-C
// 设定用户性别属性 "Gender" 为 "Male"
// `set:WithValue` 方法用于设定单个用户属性
[[SensorsAnalyticsSDK sharedInstance] set:@"Gender" to:@"Male"];

// 设定用户年龄属性 "Age" 为 18
// `set:` 方法设定一个或多个用户属性
[[SensorsAnalyticsSDK sharedInstance] set:@{@"Age" : [NSNumber numberWithInt:18]}];
```

Swift 代码示例：

```Swift
//设置单个用户属性
SensorsAnalyticsSDK.sharedInstance().set("Gender", to:"Male")

//设置多个用户属性
SensorsAnalyticsSDK.sharedInstance().set(["Age":18])
```

- 只保留初次设定的用户属性 对于需要保证只有首次设置时有效的属性，如用户首次充值金额、首次设置的昵称等，可以使用 `setOnce:` 或 `setOnce:to:` 接口进行记录。与 `set:` 方法不同的是，如果被设置的用户属性已存在，则这条记录会被忽略而不会覆盖已有数据，如果属性不存在则会自动创建。

Objective-C 代码示例：

```Objective-C
    // 设定用户 AdSource 渠道为为 "App Store"
    [[SensorsAnalyticsSDK sharedInstance] setOnce:@"AdSource" to:@"App Store"];

    // 再次设定用户 AdSource 渠道，设定无效，AdSource 属性值仍然是 "App Store"
    [[SensorsAnalyticsSDK sharedInstance] setOnce:@"AdSource" to:@"Email"];
```

Swift 代码示例：

```Swift
    SensorsAnalyticsSDK.sharedInstance().setOnce("AdSource", to: "App Store")

    SensorsAnalyticsSDK.sharedInstance().setOnce(["AdSource":"Email"])
```

- 数值类型属性累加新值 针对一些数值型属性，如消费总额、用户积分等属性，我们可以使用 `increment:` 或 `increment:by:` 对原值进行累加，神策会自动计算并保存累加之后的值。

  Objective-C 代码示例：

  ```Objective-C
    // 将用户游戏次数属性增加一次
    // increment:by: 对一个属性进行累加
    [[SensorsAnalyticsSDK sharedInstance] increment:@"GamePlayed" by:[NSNumber numberWithInt:1]];
  
    // 增加用户付费次数和积分
    // increment: 对一个或多个属性进行累加
    [[SensorsAnalyticsSDK sharedInstance] increment:@{
                                                               @"UserPaid" : [NSNumber numberWithInt:1],
                                                               @"PointEarned" : [NSNumber numberWithFloat:12.5]
                                                               }];
  ```

  Swift 代码示例：

  ```Swift
  SensorsAnalyticsSDK.sharedInstance().increment("GamePlayed", by: 1)
  SensorsAnalyticsSDK.sharedInstance().increment(["UserPaid":1,"PointEarned":5])
  ```

- 列表类型属性追加新值 对于列表类型用户属性，如用户喜爱的电影、用户点评过的餐厅等属性，可以调用 `append:by:` 接口进行追加一些新值。

Objective-C 代码示例：

```Objective-C
    // 设定用户观影列表属性，设定后属性 "Movies" 为: ["Sicario", @"Love Letter"]
    [[SensorsAnalyticsSDK sharedInstance] append:@"Movies" by:[NSSet setWithArray:@[@"Sicario", @"Love Letter"]]];

    // 再次设定该属性，属性 "Movies" 为: ["Sicario", @"Love Letter", @"Dead Poets Society"]
    [[SensorsAnalyticsSDK sharedInstance] append:@"Movies" by:[NSSet setWithArray:@[@"Love Letter", @"Dead Poets Society"]]];
```

Swift 代码示例：

```Objective-C
    SensorsAnalyticsSDK.sharedInstance().append("Movies", by: ["Sicario","Love Letter"])
```

*注1* 列表型属性中的元素必须为 `NSString` 类型，且元素的值会自动去重。关于列表型限制请见 [数据格式](https://www.sensorsdata.cn/manual/data_schema.html)7.3 属性长度限制

- 取消某个用户属性 如果需要取消已设置的某个用户属性，可以调用 `unset:` 进行取消

Objective-C 代码示例：

```Objective-C
// 取消设置 gender 属性
[[SensorsAnalyticsSDK sharedInstance] unset:@"Gender"];
```

Swift 代码示例：

```Swift
SensorsAnalyticsSDK.sharedInstance()?.unset("Gender")
```

# 具体埋点示例

事件的触发位置添加一下代码：

```Objective-C
[[SensorsAnalyticsSDK sharedInstance] track:@"web_element_click" withProperties:@{@"class_code":@"web_element_click",
                @"class_name":@"web元素点击",
                @"event_code":@"home_page_submit",
                @"event_name":@"首页提交按钮",
                @"event_page":@"首页",
                @"event_position":@"提交按钮"}];
```

上述class_code，class_name，event_code，event_name字段为必填字段，具体字段内容届时由产品提供。

用户登录：

```Objective-C
[[SensorsAnalyticsSDK sharedInstance] login:@"用户登录ID"];
```

用户注销：

```Objective-C
[[SensorsAnalyticsSDK sharedInstance] logout];
```
