### 引入 SDK

### 2.1 下载文件

从 Github 上下载 [微信小程序 sdk](https://github.com/sensorsdata/sa-sdk-miniprogram) ，sensorsdata.min.js

### 2.2 引入 SDK 、配置参数并且完成初始化

把文件放在小程序项目中，然后在 app.js 中通过 require() 引入文件 ;
调用 setPara() 方法设置初始化参数。**必须在 require() 之后，立即调用 setPara() 方法设置**；
如果异步的获取 openid 等操作已经完成，此时调用 init() 方法标志已完成初始化；
**注意**：init() 方法必须调用，否则不会发数据。

小程序 app.js 的顶部引入如下代码

```
------app.js
    var sa = require('./utils/sensorsdata.min.js');
    sa.setPara({
        name: 'sensors',
        server_url: '数据接收地址',
        // 是否开启自动采集
        autoTrack: {
            appLaunch: true,
            appShow: true,
            appHide: true,
            pageShow: true,
            pageShare: true
        },
        // 自定义渠道追踪参数，如source_channel: ["custom_param"]
        source_channel: [],
        // 是否允许控制台打印查看埋点数据(建议开启查看)
        show_log: true,
        // 是否允许修改onShareAppMessage里return的path，用来增加(用户id，分享层级，当前的path)，在app onShow中自动获取这些参数来查看具体分享来源，层级等
        allow_amend_share_path: true
    });
    sa.init();
```

setPara() 方法中更多配置参数可前往[微信小程序 SDK (标准版)使用说明](https://www.sensorsdata.cn/manual/mp_sdk_new.html)查看

### 2.3 小程序页面中采集事件

现在在其他 Page 里就可以通过 getApp() 来使用神策的追踪了

```
var app = getApp();
app.sensors.track(eventName, properties) // 第一个参数事件名 字符串类型，第二个参数 属性值 对象类型
```

![img](https://images.gitee.com/uploads/images/2020/0210/091327_9f5918c2_509471.png)



## 自定义事件追踪

[相关文档链接：](https://www.sensorsdata.cn/manual/mp_sdk.html#3-自定义事件追踪])
SDK 初始化成功后，即可以通过 app.sensors.track(event_name,properties) 记录事件：
• event_name:string，必选。表示要追踪的事件名。
• properties:object，可选。表示这个事件的属性。

例如：埋点 “ViewProduct” 事件，事件属性有商品名称，姓名等。

```
// 追踪浏览商品事件。
var app = getApp();
app.sensors.track('ViewProduct', {
    ProductName: "MacBook Pro",
    ProductPrice: 125.55
});
```

## 4. 设置用户属性

SDK 初始化成功后，即可以通过 app.sensors.setProfile(properties) 设置用户属性，如果之前存在同名属性则覆盖：
properties:object，必选。表示要设置的用户的属性。

```
var app = getApp();
app.sensors.setProfile({
    name: 'xxx',
    gender:'male'
});
```

## 5. 用户标识

[相关文档链接：](https://www.sensorsdata.cn/manual/user_identify.html#如何准确的标识用户)

在进行任何埋点之前，都应当先确定如何标识用户。**distinct_id** 是神策用来标识用户的一段唯一的字符串。

在小程序中，会有下面 3 种 id

1. 默认情况下，我们会生成一个随机数 uuid ，保存在 weixin storage 中，我们暂时称这个为 **uuid**
2. 用户打开小程序，我们可以获得用户的 weixin openid ，我们暂时称这个为 **openid**
3. 客户用户账号体系中产生或保存的真实用户 id 。我们暂时称为 **"你们服务端分配给用户具体的登录 ID"**

### 5.1 使用 openid 作为匿名 ID

如果不做任何操作，小程序会使用 uuid 作为 distinct_id ，但是这个 uuid 换了设备，或者删除小程序，会重新生成。 所以一般情况下，我们建议使用 openid 作为当前的 distinct_id。 因为获取 openid 是一个异步的操作，但是冷启动事件等会先发生，这时候这个冷启动事件的 distinct_id 就不对了。
所以我们会把先发生的操作，暂存起来，等获取到 openid 后再调用 sensors.init() 才会发数据。 下面是常见的两种获取 openid 的初始化代码。

```
-------app.js  

var sensors = require('sensorsdata.min.js');
sensors.setPara({
    name: '',
    server_url: '数据接收地址'
});

//此处代码仅供参考
wx.request({
    url: '获取 openid 的后端请求接口',
    success: function(data){
        // 如果你们能获取到openid
        sensors.setOpenid(openid);
    },
    complete: function(){
        sensors.init();
    }
});
```

**注意：init() 方法不调用就不会发送数据，只会将数据保存到队列中，确保获取 openid 成功和失败的时候都调用 init() 方法**

```
-------app.js  

var sensors = require('sensorsdata.min.js');
// 如果后端做了 appid 和 appsecret 的配置，以及 sensorsdata_conf 里有 appid 的配置
sensors.setPara({
    appid:'',
    name: '',
    server_url: '数据接收地址'
});
sensors.initWithOpenid();
```

### 5.2 匿名 ID 和用户 ID 关联

默认情况下 ，SDK 会分配一个匿名 ID (uuid) 来标识游客。当用户注册成功或登录成功时调用 login 方法，传入对应的用户 ID ；匿名 ID 会与对应的用户 ID 进行关联，关联成功之后视为同一个用户。 调用时机：注册成功、登录成功 、初始化 SDK（如果能获取到用户 ID）都需要调用 login 方法传入用户 ID。

```
-------app.js  

var sensors = require('sensorsdata.min.js');
sensors.setPara({
    name: '',
    server_url: '数据接收地址',
    // 是否开启自动采集
    autoTrack: {
	appLaunch: false,
	appShow: false,
	appHide: false,
	pageShow: false,
	pageShare: false
    }
});
// 如果能获取到"你们服务端分配给用户具体的登录 ID"，需要做用户关联情况下
sensors.login("你们服务端分配给用户具体的登录 ID");
sensors.init();
```

# 具体埋点示例

事件的触发位置添加一下代码：

~~~java
getApp().sensors.track("element_click", {
					class_code:"element_click",
					class_name:"元素点击",
					event_code:"search_item_click",
				    event_name: "搜索按钮点击",
				    event_page:"车型搜索",
					event_position:"车型搜索",  
				});
~~~

上述class_code，class_name，event_code，event_name字段为必填字段，具体字段内容届时由产品提供。

用户登录后：

~~~java
getApp().sensors.login("你们服务端分配给用户具体的登录 ID");
~~~

用户注销：

~~~java
getApp().sensors.log();
~~~
