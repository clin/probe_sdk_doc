

# 接入方式



## Gradle 编译环境（Android Studio）

**第一步：**在 **project** 级别的 build.gradle 文件中添加 Sensors Analytics android-gradle-plugin 依赖：

```android
buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:2.2.3'
        //添加 Sensors Analytics android-gradle-plugin 依赖
        classpath 'com.sensorsdata.analytics.android:android-gradle-plugin2:3.1.9'
    }
}

allprojects {
    repositories {
        jcenter()
    }
}
```

如下示例图： ![img](https://www.sensorsdata.cn/manual/img/android_sdk_autotrack_1.png)

**第二步：**在 **主 module** 的 build.gradle 文件中添加 com.sensorsdata.analytics.android 插件、Sensors Analytics SDK 依赖：

```android
apply plugin: 'com.android.application'
//添加 com.sensorsdata.analytics.android 插件
apply plugin: 'com.sensorsdata.analytics.android'

dependencies {
   //添加 Sensors Analytics SDK 依赖
   implementation files('Sgmw-SensorsAnalyticsSDK-1.0.0.aar')
}
```



如下示例图： ![img](https://www.sensorsdata.cn/manual/img/android_sdk_autotrack_2.png)



在程序的入口 **Application 的 onCreate()** 中调用 `SensorsDataAPI.startWithConfigOptions()` 在主线程中初始化 SDK。 SDK 可以自动采集一些用户行为，如 App 启动、退出、浏览页面、控件点击等。初始化 SDK 后，可以通过 `SAConfigOptions` 来设置自动采集的事件。

```java
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.sensorsdata.analytics.android.sdk.SAConfigOptions;
import com.sensorsdata.analytics.android.sdk.SensorsAnalyticsAutoTrackEventType;
import com.sensorsdata.analytics.android.sdk.SensorsDataAPI;
import org.json.JSONObject;


public class App extends Application {

    // debug 模式的数据接收地址 （测试，测试项目）
    final static String SA_SERVER_URL_DEBUG = "【测试项目】数据接收地址";

    // release 模式的数据接收地址（发版，正式项目）
    final static String SA_SERVER_URL_RELEASE = "【正式项目】数据接收地址";


    @Override
    public void onCreate() {
        super.onCreate();
        // 在 Application 的 onCreate 初始化 SDK
        initSensorsDataSDK();
    }

    /**
     * 初始化 SDK 、设置自动采集、设置公共属性
     */
    private void initSensorsDataSDK() {
        try {
            // 设置 SAConfigOptions，传入数据接收地址 SA_SERVER_URL
            SAConfigOptions saConfigOptions = new SAConfigOptions(isDebugMode(this) ? SA_SERVER_URL_DEBUG : SA_SERVER_URL_RELEASE);

            // 通过 SAConfigOptions 设置神策 SDK 自动采集 options
            saConfigOptions.setAutoTrackEventType(
                            SensorsAnalyticsAutoTrackEventType.APP_START | // 自动采集 App 启动事件
                            SensorsAnalyticsAutoTrackEventType.APP_END | // 自动采集 App 退出事件
                            SensorsAnalyticsAutoTrackEventType.APP_VIEW_SCREEN | // 自动采集 App 浏览页面事件
                            SensorsAnalyticsAutoTrackEventType.APP_CLICK)   // 自动采集控件点击事件
                    .enableLog(isDebugMode(this))        // 开启神策调试日志，默认关闭(调试时，可开启日志)。
                    .enableTrackAppCrash(); // 开启 crash 采集

            // 需要在主线程初始化神策 SDK
            SensorsDataAPI.startWithConfigOptions(this, saConfigOptions);

            // 初始化 SDK 后，可以获取应用名称设置为公共属性
            JSONObject properties = new JSONObject();
            properties.put("appName", getAppName(this));
            SensorsDataAPI.sharedInstance().registerSuperProperties(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param context App 的 Context
     *                获取应用程序名称
     */
    public static CharSequence getAppName(Context context) {
        if (context == null) {
            return "";
        }
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                return "";
            }
            ApplicationInfo appInfo = packageManager.getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA);
            return appInfo.loadLabel(packageManager);
        } catch (Exception e) {
             e.printStackTrace();
        }
        return "";
    }

    /**
     * @param context App 的 Context
     * @return debug return true,release return false
     * 用于判断是 debug 包，还是 relase 包
     */
    public static boolean isDebugMode(Context context) {
        try {
            ApplicationInfo info = context.getApplicationInfo();
            return (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
```

## 埋点示例

### 3.1 追踪事件

可以通过 `track()` 方法追踪用户行为事件，并为事件添加自定义属性（触发的事件会存储到神策分析系统的 events 表中）。

```java
    try {
        JSONObject properties = new JSONObject();
        properties.put("searchKeyWord", "神策数据");// 搜索关键字
        // 记录搜索（search ）事件
        SensorsDataAPI.sharedInstance().track("search", properties);
    } catch (JSONException e) {
        e.printStackTrace();
    }
```

### 3.2 设置用户属性

为了更准确地提供针对人群的分析服务，可以使用神策分析 SDK 的 profileSet() 等方法设置用户属性，如年龄、性别等。用户可以在留存分析、分布分析等功能中，使用用户属性作为过滤条件，精确分析特定人群的指标。 （设置的用户属性会存储到神策分析系统的 users 表中）

```java
    try {
        JSONObject properties = new JSONObject();
        properties.put("email", "xxx@xxx.xx");
        // 设置用户的 email
        SensorsDataAPI.sharedInstance().profileSet(properties);
    } catch (JSONException e) {
        e.printStackTrace();
    }
```

### 3.3 匿名 ID 和登录 ID 关联

[如何准确的标识用户](https://www.sensorsdata.cn/manual/user_identify.html)

成功关联**设备 ID** 和**登录 ID** 之后，用户在该**设备 ID** 上或该**登录 ID** 下的行为就会贯通，被认为是一个**神策 ID** 发生的。在进行事件、漏斗、留存等用户相关分析时也会算作一个用户。

关联**设备 ID** 和**登录 ID** 的方法虽然实现了更准确的用户追踪，但是也会增加埋点接入的复杂度。所以一般来说，我们建议只有当同时满足以下条件时，才考虑进行 ID 关联：

1. 需要贯通一个用户在一个设备上注册前后的行为。
2. 需要贯通一个注册用户在不同设备上登录之后的行为

用户在登录前 ，SDK 会分配一个匿名 ID 来标识游客。当用户注册成功或登录成功时调用 login 方法，传入对应的登录 ID ；匿名 ID 会与对应的登录 ID 进行关联，关联成功之后视为同一个用户。 调用时机：注册成功、登录成功 、初始化 SDK（如果能获取到登录 ID）都需要调用 login 方法传入登录 ID。

**注意：**登录 ID 是指可以唯一标识一个用户的 ID，通常是业务数据库里的主键或其它唯一标识

```
//注册成功、登录成功、初始化SDK后  调用 login 传入登录 ID
SensorsDataAPI.sharedInstance().login("你们服务端分配给用户具体的登录 ID");
```



# 具体埋点示例

事件的触发位置添加一下代码：

~~~java
try {
    JSONObject properties = new JSONObject();
    properties.put("class_code", "web_element_click");
    properties.put("class_name", "web元素点击");
    properties.put("event_code", "home_page_submit");
    properties.put("event_name", "首页提交按钮");
    properties.put("event_page", "首页");
    properties.put("event_position", "提交按钮");
    properties.put("element_type", "。。。");
    properties.put("element_link_url", "。。。");
    SensorsDataAPI.sharedInstance().track("web_element_click", properties);
} catch (JSONException e) {
    e.printStackTrace();
}
~~~

上述class_code，class_name，event_code，event_name字段为必填字段，具体字段内容届时由产品提供。

